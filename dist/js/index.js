$(document).ready(function () {
    $(".move_down").on("click", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();

        //забираем идентификатор бока с атрибута href
        var id = $(this).attr('href'),

            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top;

        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({ scrollTop: top }, 1000);
    });
    $("#up_arrow").on("click", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top }, 1000);
    });
    $('.slider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });
    $('.four_part_slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        arrows: false

    });
    $('.seven_slide').slick({
        dots: true,
        infinite: true,
        speed: 800,
        slidesToShow: 2,
        centerMode: true,
        variableWidth: true,
        autoplay: false,
        autoplaySpeed: 6000,
        /* focusOnSelect: true, */
        arrows: false


    });

});




wow = new WOW(
    {
        boxClass: 'wow',      // default
        animateClass: 'animated', // default
        offset: 0,          // default
        mobile: true,       // default
        live: true        // default
    }
)
wow.init(); 



var switch_menu = function () {

    var element1 = document.getElementById("burger_open");
    var element2 = document.getElementById("burger_close");
    var element3 = document.getElementById("header_nav");
    element1.classList.toggle("fa-bars_on");
    element2.classList.toggle("fa-times_off");
    element3.classList.toggle("header_nav_open");
    element3.classList.toggle("header_nav_close");

}


    
    var windowWidth = $(window).width();
    if (windowWidth < 800){
        $(".container .third_icons .icon_item").removeClass("wow")
        $(".container .third_part_content").removeClass("wow")
    }else{
        $(".container .third_icons .icon_item").addClass("wow")
        $(".container .third_part_content").addClass("wow")
    }
